from django.db import models


class Tournament(models.Model):
    name = models.CharField(max_length=32)
    start_at = models.DateTimeField(null=True)

    def __str__(self):
        return f'Tournament {self.name}'
