import os

__all__ = (
    'LOGGING',
    'CUSTOM_LOGGER_NAME',
)

CUSTOM_LOGGER_NAME = 'custom_logger'

LOG_DIR = 'logs'
if not os.path.exists(LOG_DIR):
    os.mkdir(LOG_DIR)

LOGGING = {
    'loggers': {
        '': {
            'handlers': ['debug_handler', 'error_handler'],
        },
        CUSTOM_LOGGER_NAME: {
            'handlers': [
                'custom_handler',
            ],
            'level': 'DEBUG',
            'propagate': False,
        },
    },
    'handlers': {
        'custom_handler': {
            'class': 'logging.FileHandler',
            'formatter': 'default',
            'level': 'DEBUG',
            'filename': f'{LOG_DIR}/custom.log',
        },
        'debug_handler': {
            'class': 'logging.FileHandler',
            'formatter': 'default',
            'level': 'DEBUG',
            'filename': f'{LOG_DIR}/debug.log',
        },
        'error_handler': {
            'class': 'logging.FileHandler',
            'formatter': 'default',
            'level': 'ERROR',
            'filename': f'{LOG_DIR}/error.log',
        },
    },
    'formatters': {
        'default': {
            'datefmt': '%d/%b/%Y %H:%M:%S',
            'format': '[%(levelname)s] %(asctime)s %(name)s.%(funcName)s: %(message)s'
        }
    },
    'version': 1,
    'disable_existing_loggers': True,
}
