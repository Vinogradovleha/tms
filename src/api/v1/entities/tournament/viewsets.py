from rest_framework import viewsets

from api.v1.entities.tournament.filters import TournamentFilter
from api.v1.entities.tournament.serializers import TournamentSerializer
from tournament.models import Tournament


class TournamentViewSet(viewsets.ModelViewSet):
    queryset = Tournament.objects.all()
    serializer_class = TournamentSerializer
    filter_class = TournamentFilter
