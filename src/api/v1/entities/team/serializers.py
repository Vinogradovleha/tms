from rest_framework import serializers

from api.v1.entities.tournament.serializers import TournamentSerializer
from team.models import Team


class TeamSerializer(serializers.ModelSerializer):
    tournament = TournamentSerializer()
    tournament_id = serializers.IntegerField(required=False)

    class Meta:
        model = Team
        exclude = ()

    def create(self, validated_data):
        tournament_data = validated_data.pop('tournament')

        serializer = TournamentSerializer(data=tournament_data)
        serializer.is_valid()
        tournament = serializer.save()

        validated_data['tournament_id'] = tournament.pk

        return super().create(validated_data=validated_data)
