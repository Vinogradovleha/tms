from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.status import HTTP_204_NO_CONTENT
from rest_framework.viewsets import ModelViewSet

from api.v1.entities.match.serializers import MatchSerializer, MatchWinnerSerializer
from match.models import Match
from team.models import Team


class MatchViewSet(ModelViewSet):
    queryset = Match.objects.all()
    serializer_class = MatchSerializer

    @action(detail=True, methods=['put'])
    def set_winner(self, request, pk=None):
        winner_serializer = MatchWinnerSerializer(data=request.data)
        winner_serializer.is_valid(raise_exception=True)

        winner_team = Team.objects.get(pk=winner_serializer.data['winner_team_id'])

        Match.objects.filter(pk=pk).update(
            winner=winner_team,
            status=Match.STATUSES.COMPLETED.name
        )

        return Response(status=HTTP_204_NO_CONTENT)
