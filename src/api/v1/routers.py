from rest_framework.routers import DefaultRouter

from api.v1.entities.match.viewsets import MatchViewSet
from api.v1.entities.team.viewsets import TeamViewSet
from api.v1.entities.tournament.viewsets import TournamentViewSet

api_router = DefaultRouter()
api_router.register('tournaments', TournamentViewSet)
api_router.register('teams', TeamViewSet)
api_router.register('matches', MatchViewSet)
