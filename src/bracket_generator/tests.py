import json
from uuid import uuid4

from django.test import TestCase
from django.urls import reverse

from bracket_generator.manager import BracketGenerator
from match.models import Match
from team.models import Team
from tournament.models import Tournament


class GenerateBracketsTest(TestCase):
    @staticmethod
    def get_matches_list_url():
        return reverse('match-list')

    @staticmethod
    def get_set_winner_url(match_id):
        return reverse(f'match-set-winner', kwargs={'pk': match_id})

    @staticmethod
    def generate_name():
        return uuid4().hex

    def generate_teams(self, tournament, count=4):
        team_list = []

        for _ in range(count):
            team = Team(tournament=tournament, name=self.generate_name())
            team_list.append(team)

        Team.objects.bulk_create(team_list, batch_size=500)

    def test_generate_brackets(self):
        tournament = Tournament.objects.create(name='test_generate_brackets')

        self.generate_teams(tournament=tournament, count=4)

        generator = BracketGenerator(tournament=tournament)
        generator.generate_brackets()

        matches_resp = self.client.get(self.get_matches_list_url())
        matches_list_jsn = json.loads(matches_resp.content)

        for match_jsn in matches_list_jsn:
            match_id = match_jsn['id']
            request_data = {'winner_team_id': match_jsn['teams'][0]}

            self.client.put(
                path=self.get_set_winner_url(match_id),
                data=json.dumps(request_data),
                content_type='application/json'
            )

        matches_resp = self.client.get(self.get_matches_list_url())
        matches_list_jsn = json.loads(matches_resp.content)

        for match_jsn in matches_list_jsn:
            self.assertEqual(match_jsn['status'], Match.STATUSES.COMPLETED.name)
            self.assertIsNotNone(match_jsn['winner'])
