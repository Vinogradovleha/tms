from django.conf.urls import url
from django.urls import include

from api.v1.routers import api_router
from urls.admin import urlpatterns as admin_urls

urlpatterns = [
    url(r'api/v1/', include(api_router.urls))
]
urlpatterns.extend(admin_urls)
