from django.contrib import admin

from match.models import Match


class MatchAdmin(admin.ModelAdmin):
    ...


admin.site.register(Match, MatchAdmin)
