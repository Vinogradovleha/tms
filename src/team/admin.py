from django.contrib import admin

from team.models import Team


class TeamAdmin(admin.ModelAdmin):
    search_fields = ('name', )


admin.site.register(Team, TeamAdmin)
