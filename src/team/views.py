from django.http import HttpResponse

from team.models import Team
from tournament.models import Tournament


def create_team(request, tournament_id, name):
    tournament = Tournament.objects.filter(pk=tournament_id).first()

    if not tournament:
        res = 'Tournament not found'
    else:
        team = Team.objects.create(name=name, tournament=tournament)
        res = team.pk

    return HttpResponse(res)
