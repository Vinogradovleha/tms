# TMS

* **Clone git repository:**
    ```
    git clone https://gitlab.com/Vinogradovleha/tms
    ```

* **Make sure your local virtual_env Python version is 3.7.6:**
    ```
    virtualenv -p python3 venv_tms_be
    source venv_tms_be/bin/activate
    ```

* **Make sure all required packages installed. From src folder:**
    ```
    pip install -r .meta/packages
    ```

* **Run migrations:**
    ```
    ./manage.py migrate
    ```

* **Run server:**
    ```
    ./manage.py runserver
    ```
